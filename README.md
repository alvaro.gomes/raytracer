# Raytracer da cadeira de Computação Grafica
Um raytracer simples com refração e reflexão.

# Configurar o ambiente de build
Esse comando só precisa ser executado uma vez em cada maquina ou quando o
arquivo Dockerfile-build for modificado. Ele cria uma imagem docker chamada
`build-raytracer` com todas as ferramentas e bibliotecas necessárias para
desenvolver esse projeto que será utilizado como ambiente de desenvolvimento
desse projeto:

    docker build -f Dockerfile-build -t build-raytracer .

# Executando as builds
Para rodar uma build desse software utilize o comando:

    docker run -it --rm -v $PWD:/code build-raytracer

Isso equivale a executar o comando `make` dentro do container. Outros
argumentos ou alvos podem ser passados ao final do comando para maior controle
sobre seu processo de desenvolvimento:

    docker run -it --rm -v $PWD:/code build-teste VAR=teste clean
