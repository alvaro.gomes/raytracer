.PHONY: run clean
#.SILENT:

# Variaveis expostas (podem ser sobreescritas)
SRC_FOLDER=src
BIN_FOLDER=bin
BIN_NAME=raytracer

CXX=g++
CXXFLAGS=--std=c++11 -g -Og

# Deveriam ser gerados automaticamente
override FILES=$(basename $(shell find $(SRC_FOLDER) -name *.cpp -printf "%P\n"))

# Variaveis internas (usadas para organizacao)
override BUILD=$(CXX) $(CXXFLAGS)
override COMPILE=$(CXX) -c $(CXXFLAGS)
override OUTPUT=$(BIN_FOLDER)/$(BIN_NAME)
override OBJ=$(patsubst %,$(BIN_FOLDER)/%.o,$(FILES))
override DEPS=$(shell find $(SRC_FOLDER) -name *.h)

# Regras de build
$(BIN_FOLDER)/%.o: $(SRC_FOLDER)/%.cpp $(DEPS)
	mkdir -p $(BIN_FOLDER)/$(dir $(subst src/,,$<))
	$(COMPILE) -o $@ $<

$(OUTPUT): $(OBJ)
	$(BUILD) -o $@ $^

run: $(OUTPUT)
	./$(OUTPUT)

clean:
	rm -r $(BIN_FOLDER)
